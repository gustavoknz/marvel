package kieling.marvel;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.File;

import kieling.marvel.deps.DaggerDeps;
import kieling.marvel.deps.Deps;
import kieling.marvel.networking.NetworkModule;

public class BaseActivity extends AppCompatActivity {
    Deps deps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();
    }

    protected Deps getDeps() {
        return deps;
    }
}
