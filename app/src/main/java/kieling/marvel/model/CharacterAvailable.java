package kieling.marvel.model;

import com.google.gson.annotations.SerializedName;

public class CharacterAvailable {
    @SerializedName("available")
    private int available;

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "CharacterAvailable{" +
                "available=" + available +
                '}';
    }
}
