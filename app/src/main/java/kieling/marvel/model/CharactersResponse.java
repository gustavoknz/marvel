package kieling.marvel.model;

import com.google.gson.annotations.SerializedName;

public class CharactersResponse {
    @SerializedName("code")
    private int code;
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private CharactersData data;

    public CharactersResponse() {
    }

    public CharactersResponse(int code, String status, CharactersData data) {
        this.code = code;
        this.status = status;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CharactersData getData() {
        return data;
    }

    public void setData(CharactersData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CharactersResponse{" +
                "code=" + code +
                ", status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
