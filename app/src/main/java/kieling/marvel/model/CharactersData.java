package kieling.marvel.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class CharactersData {
    @SerializedName("results")
    private CharacterResult[] results;

    public CharactersData() {
    }

    public CharactersData(CharacterResult[] results) {
        this.results = results;
    }

    public CharacterResult[] getResults() {
        return results;
    }

    public void setResults(CharacterResult[] results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "CharactersData{results=" + Arrays.toString(results) + "'}";
    }
}
