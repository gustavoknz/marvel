package kieling.marvel.model;

import com.google.gson.annotations.SerializedName;

public class CharacterResult {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("thumbnail")
    private CharacterThumbnail characterThumbnail;
    @SerializedName("resourceURI")
    private String resourceURI;
    @SerializedName("comics")
    private CharacterAvailable comics;
    @SerializedName("series")
    private CharacterAvailable series;
    @SerializedName("stories")
    private CharacterAvailable stories;
    @SerializedName("events")
    private CharacterAvailable events;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CharacterThumbnail getCharacterThumbnail() {
        return characterThumbnail;
    }

    public void setCharacterThumbnail(CharacterThumbnail characterThumbnail) {
        this.characterThumbnail = characterThumbnail;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public CharacterAvailable getComics() {
        return comics;
    }

    public void setComics(CharacterAvailable comics) {
        this.comics = comics;
    }

    public CharacterAvailable getSeries() {
        return series;
    }

    public void setSeries(CharacterAvailable series) {
        this.series = series;
    }

    public CharacterAvailable getStories() {
        return stories;
    }

    public void setStories(CharacterAvailable stories) {
        this.stories = stories;
    }

    public CharacterAvailable getEvents() {
        return events;
    }

    public void setEvents(CharacterAvailable events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "CharacterResult{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", characterThumbnail=" + characterThumbnail +
                ", resourceURI='" + resourceURI + '\'' +
                ", comics=" + comics +
                ", series=" + series +
                ", stories=" + stories +
                ", events=" + events +
                '}';
    }
}
