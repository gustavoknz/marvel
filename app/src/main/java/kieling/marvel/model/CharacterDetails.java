package kieling.marvel.model;

import java.io.Serializable;

public class CharacterDetails implements Serializable {
    private String name;
    private String description;
    private String thumbnailPath;
    private int comics;
    private int series;
    private int stories;
    private int events;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public int getComics() {
        return comics;
    }

    public void setComics(int comics) {
        this.comics = comics;
    }

    public int getSeries() {
        return series;
    }

    public void setSeries(int series) {
        this.series = series;
    }

    public int getStories() {
        return stories;
    }

    public void setStories(int stories) {
        this.stories = stories;
    }

    public int getEvents() {
        return events;
    }

    public void setEvents(int events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "CharacterDetails{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", thumbnailPath='" + thumbnailPath + '\'' +
                ", comics=" + comics +
                ", series=" + series +
                ", stories=" + stories +
                ", events=" + events +
                '}';
    }
}
