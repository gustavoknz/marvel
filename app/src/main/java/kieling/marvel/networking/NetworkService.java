package kieling.marvel.networking;

import io.reactivex.Single;
import kieling.marvel.model.CharactersResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NetworkService {
    @GET("characters")
    Single<CharactersResponse> getCharactersData(@Query("offset") long offset,
                                                 @Query("ts") long timestamp,
                                                 @Query("apikey") String publicKey,
                                                 @Query("hash") String hash);

    @GET("characters/{characterId}")
    Single<CharactersResponse> getCharacterDetails(@Path("characterId") long id,
                                                   @Query("ts") long timestamp,
                                                   @Query("apikey") String publicKey,
                                                   @Query("hash") String hash);
}
