package kieling.marvel.networking;

import com.google.gson.annotations.SerializedName;

class Response {
    @SerializedName("status")
    private String status;

    @SuppressWarnings({"unused", "used by Retrofit"})
    public Response() {
    }

    public Response(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}