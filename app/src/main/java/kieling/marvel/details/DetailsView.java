package kieling.marvel.details;

import kieling.marvel.helper.GeneralView;
import kieling.marvel.model.CharacterDetails;

public interface DetailsView extends GeneralView {
    void onSuccess(CharacterDetails characterDetails);
}
