package kieling.marvel.details;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.marvel.BaseActivity;
import kieling.marvel.R;
import kieling.marvel.helper.Constants;
import kieling.marvel.model.CharacterDetails;
import kieling.marvel.networking.NetworkService;

public class DetailsActivity extends BaseActivity implements DetailsView {
    private static final String TAG = "DetailsActivity";
    @Inject
    NetworkService service;
    @BindColor(R.color.colorAccent)
    int colorAccent;
    @BindView(R.id.detailsErrorText)
    TextView errorText;
    @BindView(R.id.detailsProgressBar)
    ProgressBar progressBar;
    @BindView(R.id.detailsThumbnail)
    ImageView thumbnail;
    @BindView(R.id.detailsNameText)
    TextView nameText;
    @BindView(R.id.detailsDescriptionText)
    TextView descriptionText;
    @BindView(R.id.detailsComicsText)
    TextView comicsText;
    @BindView(R.id.detailsSeriesText)
    TextView seriesText;
    @BindView(R.id.detailsStoriesText)
    TextView storiesText;
    @BindView(R.id.detailsEventsText)
    TextView eventsText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        getDeps().inject(this);

        Log.d(TAG, "savedInstanceState = " + savedInstanceState);
        progressBar.getIndeterminateDrawable().setColorFilter(colorAccent, PorterDuff.Mode.MULTIPLY);
        if (savedInstanceState == null) {
            int id = getIntent().getIntExtra(Constants.PARAM_CHARACTER_ID_KEY, -1);
            if (id > 0) {
                new DetailsPresenter(service, this).getCharacterDetails(id);
            } else {
                Log.e(TAG, "Weird: could not get parameter");
                removeWait();
                onFailure();
            }
        }
    }

    @Override
    public void showWait() {
        Log.d(TAG, "in showWait...");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        Log.d(TAG, "in removeWait...");
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onFailure() {
        Log.d(TAG, "in onFailure");
        errorText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(CharacterDetails details) {
        Log.d(TAG, "in onSuccess: " + details);
        Picasso.get()
                .load(details.getThumbnailPath())
                .placeholder(R.drawable.ic_thumbnail_placeholder)
                .error(R.drawable.ic_thumbnail_placeholder)
                .into(thumbnail);
        nameText.setText(details.getName());
        if ("".equals(details.getDescription())) {
            descriptionText.setText(R.string.details_description_empty);
        } else {
            descriptionText.setText(details.getDescription());
        }
        comicsText.setText(getString(R.string.details_comics, details.getComics()));
        seriesText.setText(getString(R.string.details_series, details.getSeries()));
        storiesText.setText(getString(R.string.details_stories, details.getStories()));
        eventsText.setText(getString(R.string.details_events, details.getEvents()));
    }
}
