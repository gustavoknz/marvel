package kieling.marvel.details;

import android.util.Log;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import kieling.marvel.BuildConfig;
import kieling.marvel.helper.Utils;
import kieling.marvel.model.CharacterDetails;
import kieling.marvel.model.CharacterResult;
import kieling.marvel.model.CharactersResponse;
import kieling.marvel.networking.NetworkService;

class DetailsPresenter {
    private static final String TAG = "MainPresenter";
    private final NetworkService service;
    private final DetailsView detailsView;
    private final CompositeDisposable compositeDisposable;

    DetailsPresenter(NetworkService service, DetailsView detailsView) {
        this.service = service;
        this.detailsView = detailsView;
        this.compositeDisposable = new CompositeDisposable();
    }

    void getCharacterDetails(int id) {
        detailsView.showWait();

        long timestamp = System.currentTimeMillis();
        String hash = Utils.getHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);
        String publicKey = BuildConfig.PUBLIC_KEY;
        Log.d(TAG, "id: " + id + "; timestamp: " + timestamp + "; publicKey: " + publicKey + "; hash: " + hash);
        compositeDisposable.add(service.getCharacterDetails(id, timestamp, publicKey, hash)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn((Throwable t) -> {
                    Log.e(TAG, "Error fetching data", t);
                    detailsView.onFailure();
                    return new CharactersResponse();
                })
                .map(charactersResponse -> {
                    CharacterDetails details = new CharacterDetails();
                    if (charactersResponse.getData() != null && charactersResponse.getData().getResults() != null && charactersResponse.getData().getResults().length > 0) {
                        CharacterResult result = charactersResponse.getData().getResults()[0];
                        details.setName(result.getName());
                        details.setThumbnailPath(String.format("%s/landscape_incredible.%s", result.getCharacterThumbnail().getPath(), result.getCharacterThumbnail().getExtension()));
                        details.setDescription(result.getDescription());
                        details.setComics(result.getComics().getAvailable());
                        details.setSeries(result.getSeries().getAvailable());
                        details.setStories(result.getStories().getAvailable());
                        details.setEvents(result.getEvents().getAvailable());
                    }
                    return details;
                })
                .subscribe(characterDetails -> {
                    detailsView.removeWait();
                    detailsView.onSuccess(characterDetails);
                }));
    }

    void onStop() {
        compositeDisposable.clear();
    }

}
