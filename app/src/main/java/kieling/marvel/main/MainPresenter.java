package kieling.marvel.main;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import kieling.marvel.BuildConfig;
import kieling.marvel.helper.Utils;
import kieling.marvel.model.CharacterItem;
import kieling.marvel.model.CharacterResult;
import kieling.marvel.model.CharactersResponse;
import kieling.marvel.networking.NetworkService;

class MainPresenter {
    private static final String TAG = "MainPresenter";
    private final NetworkService service;
    private final MainView mainView;
    private final CompositeDisposable compositeDisposable;

    MainPresenter(NetworkService service, MainView mainView) {
        this.service = service;
        this.mainView = mainView;
        this.compositeDisposable = new CompositeDisposable();
    }

    void getCharactersData(int offset) {
        mainView.showWait();

        long timestamp = System.currentTimeMillis();
        String hash = Utils.getHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);
        String publicKey = BuildConfig.PUBLIC_KEY;
        Log.d(TAG, "offset: " + offset + "; timestamp: " + timestamp + "; publicKey: " + publicKey + "; hash: " + hash);
        compositeDisposable.add(service.getCharactersData(offset, timestamp, publicKey, hash)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn((Throwable t) -> {
                    Log.e(TAG, "Error fetching data", t);
                    mainView.onFailure();
                    return new CharactersResponse();
                })
                .map(charactersResponse -> {
                    CharacterItem item;
                    List<CharacterItem> items = new ArrayList<>();
                    if (charactersResponse.getData() != null && charactersResponse.getData().getResults() != null) {
                        for (CharacterResult result : charactersResponse.getData().getResults()) {
                            item = new CharacterItem();
                            item.setId(result.getId());
                            item.setThumbnailPath(String.format("%s/portrait_small.%s", result.getCharacterThumbnail().getPath(), result.getCharacterThumbnail().getExtension()));
                            item.setName(result.getName());
                            item.setDescription(result.getDescription());
                            items.add(item);
                        }
                    }
                    return items;
                })
                .subscribe(characterItemList -> {
                    mainView.removeWait();
                    mainView.onSuccess(characterItemList);
                }));
    }

    void onStop() {
        compositeDisposable.clear();
    }
}
