package kieling.marvel.main;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.marvel.R;
import kieling.marvel.details.DetailsActivity;
import kieling.marvel.helper.Constants;
import kieling.marvel.model.CharacterItem;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private static final String TAG = "MainAdapter";
    private final List<CharacterItem> characterItemList;
    private final Context context;

    MainAdapter(List<CharacterItem> characterItemList, Context context) {
        this.characterItemList = characterItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_character, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.ViewHolder holder, int position) {
        final CharacterItem item = characterItemList.get(position);
        if (position % 2 == 0) {
            holder.itemLayout.setBackgroundResource(android.R.color.white);
        } else {
            holder.itemLayout.setBackgroundResource(R.color.gray);
        }
        Picasso.get()
                .load(item.getThumbnailPath())
                .placeholder(R.drawable.ic_thumbnail_placeholder)
                .error(R.drawable.ic_thumbnail_placeholder)
                .into(holder.thumbnail);
        holder.name.setText(item.getName());
        holder.description.setText(item.getDescription());
        holder.itemLayout.setOnClickListener(v -> {
            Log.d(TAG, "item clicked. id = " + item.getId());
            Intent intent = new Intent(v.getContext(), DetailsActivity.class);
            intent.putExtra(Constants.PARAM_CHARACTER_ID_KEY, item.getId());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return characterItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.itemLayout)
        View itemLayout;
        @BindView(R.id.itemCharacterThumbnail)
        ImageView thumbnail;
        @BindView(R.id.itemCharacterName)
        TextView name;
        @BindView(R.id.itemCharacterDescription)
        TextView description;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick " + getAdapterPosition() + " " + characterItemList.get(getAdapterPosition()).getId());
        }
    }
}
