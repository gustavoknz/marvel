package kieling.marvel.main;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import kieling.marvel.BaseActivity;
import kieling.marvel.R;
import kieling.marvel.model.CharacterItem;
import kieling.marvel.networking.NetworkService;

public class MainActivity extends BaseActivity implements MainView {
    private static final String TAG = "MainActivity";
    private static final String LIST_STATE_KEY = "listStateKey";
    @Inject
    NetworkService service;
    @BindColor(R.color.colorAccent)
    int colorAccent;
    @BindView(R.id.mainErrorText)
    TextView errorText;
    @BindView(R.id.mainList)
    RecyclerView recyclerView;
    @BindView(R.id.mainProgressBar)
    ProgressBar progressBar;
    private MainPresenter presenter;
    private RecyclerView.Adapter mAdapter;
    private List<CharacterItem> mItemsList;
    private LinearLayoutManager mLayoutManager;
    private Parcelable mListState;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getDeps().inject(this);

        Log.d(TAG, "savedInstanceState = " + savedInstanceState);
        progressBar.getIndeterminateDrawable().setColorFilter(colorAccent, PorterDuff.Mode.MULTIPLY);
        if (savedInstanceState == null) {
            presenter = new MainPresenter(service, this);
            presenter.getCharactersData(0);

            mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            mItemsList = new ArrayList<>();
            mAdapter = new MainAdapter(mItemsList, getApplicationContext());
        }
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new EndlessScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView recyclerView) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                presenter.getCharactersData(totalItemsCount);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mListState != null) {
            mLayoutManager.onRestoreInstanceState(mListState);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        // Save list state
        mListState = mLayoutManager.onSaveInstanceState();
        state.putParcelable(LIST_STATE_KEY, mListState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);

        // Retrieve list state and list/item positions
        if (state != null) {
            mListState = state.getParcelable(LIST_STATE_KEY);
        }
    }

    @Override
    public void showWait() {
        Log.d(TAG, "in showWait...");
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        Log.d(TAG, "in removeWait...");
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFailure() {
        Log.d(TAG, "in onFailure");
        errorText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(List<CharacterItem> newItemsList) {
        Log.d(TAG, "in onSuccess: " + newItemsList);
        if (newItemsList.size() > 0) {
            mItemsList.addAll(newItemsList);
            mAdapter.notifyDataSetChanged();
        } else {
            errorText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }
}
