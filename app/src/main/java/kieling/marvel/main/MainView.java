package kieling.marvel.main;

import java.util.List;

import kieling.marvel.helper.GeneralView;
import kieling.marvel.model.CharacterItem;

interface MainView extends GeneralView {
    void onSuccess(List<CharacterItem> charactersInfoList);
}
