package kieling.marvel.helper;

import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class Utils {
    private static final String TAG = "Utils";

    public static String getHash(long timestamp, String privateKey, String publicKey) {
        String str = String.format(Locale.getDefault(), "%d%s%s", timestamp, privateKey, publicKey);
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(str.getBytes());
            byte messageDigest[] = digest.digest();
            BigInteger bigInt = new BigInteger(1, messageDigest);
            StringBuilder hashText = new StringBuilder(bigInt.toString(16));
            while (hashText.length() < 32) {
                hashText.insert(0, "0");
            }
            return hashText.toString();
        } catch (NoSuchAlgorithmException nsae) {
            Log.e(TAG, "Error generating hash", nsae);
            return null;
        }
    }
}
