package kieling.marvel.helper;

public class Constants {
    public static final String PARAM_CHARACTER_ID_KEY = "paramCharacterId";
    public static final String CHARACTER_DETAILS_INSTANCE_KEY = "detailsObject";
}
