package kieling.marvel.helper;

public interface GeneralView {
    void showWait();

    void removeWait();

    void onFailure();
}
