package kieling.marvel.deps;

import javax.inject.Singleton;

import dagger.Component;
import kieling.marvel.details.DetailsActivity;
import kieling.marvel.main.MainActivity;
import kieling.marvel.networking.NetworkModule;

@Singleton
@Component(modules = {NetworkModule.class})
public interface Deps {
    void inject(MainActivity mainActivity);

    void inject(DetailsActivity detailsActivity);
}
